package br.com.projeto.login.config;

import java.net.URI;
import java.net.URISyntaxException;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DatabaseConfig {

	@Value("${spring.datasource.url}")
	private String dbUrl;

	@Bean
	public DataSource dataSource() {

		DataSource basicDataSource;

		try {
			basicDataSource = databaseURL();
		} catch (Exception e) {

			HikariConfig config = new HikariConfig();
			config.setJdbcUrl(dbUrl);
			basicDataSource = new HikariDataSource(config);

		}

		return basicDataSource;

	}

	private BasicDataSource databaseURL() throws URISyntaxException {

		String url = System.getenv("DATABASE_URL");
		URI dbUri = new URI(url);
		String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();

		BasicDataSource basicDataSource = new BasicDataSource();

		basicDataSource.setUrl(dbUrl);
		basicDataSource.setUsername(dbUri.getUserInfo().split(":")[0]);
		basicDataSource.setPassword(dbUri.getUserInfo().split(":")[1]);

		return basicDataSource;

	}
}