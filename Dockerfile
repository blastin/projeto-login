#IMAGE
FROM alpine:latest

#ATUALIZAR ALPINE
RUN apk update

#BAIXANDO OPENJDK-8-JRE
RUN apk add openjdk8-jre

#CRIANDO USUARIO
RUN adduser -D servidor

#SETAR PARA SERVIDOR
USER servidor

#SETANDO PARA WORKSPACE SERVIDOR
WORKDIR /home/servidor/

#COPIANDO TARGET PROJETO
COPY target/*.jar deploy.jar

#COMANDO INICIAL
CMD ["/usr/bin/java", "-jar", "deploy.jar"]
