package br.com.projeto.login.test.servico;

import javax.validation.ConstraintViolationException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.projeto.login.excessao.UsuarioJahExisteException;
import br.com.projeto.login.modelo.Usuario;
import br.com.projeto.login.servico.Servico;
import br.com.projeto.login.test.UsuarioFabrica;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServicoTest {

	@Autowired
	private Servico servico;

	@Test
	public void cadastrarUsuarioTest() throws UsuarioJahExisteException {

		Usuario usuario = UsuarioFabrica.fabricar("cadastrarUsuarioTest", "password");

		Assert.assertEquals(usuario, servico.cadastrarUsuario(usuario));

	}

	@Test(expected = ConstraintViolationException.class)
	public void cadastrarUsuarioInvalido1Test() throws UsuarioJahExisteException {

		Usuario usuario = UsuarioFabrica.fabricar("", "");

		Assert.assertEquals(usuario, servico.cadastrarUsuario(usuario));

	}

	@Test(expected = ConstraintViolationException.class)
	public void cadastrarUsuarioInvalido2Test() throws UsuarioJahExisteException {

		Usuario usuario = UsuarioFabrica.fabricar("a", "b");

		Assert.assertEquals(usuario, servico.cadastrarUsuario(usuario));

	}

	@Test(expected = UsuarioJahExisteException.class)
	public void cadastrarUsuarioDuplicadoTest() throws UsuarioJahExisteException {

		Usuario usuario = UsuarioFabrica.fabricar("cadastrarUsuarioDuplicadoTest", "SENHA");

		Assert.assertEquals(usuario, servico.cadastrarUsuario(usuario));
		Assert.assertEquals(usuario, servico.cadastrarUsuario(usuario));

	}

}
