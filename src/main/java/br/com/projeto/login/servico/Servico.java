package br.com.projeto.login.servico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projeto.login.enumeracao.Protocolo;
import br.com.projeto.login.excessao.UsuarioJahExisteException;
import br.com.projeto.login.modelo.Usuario;
import br.com.projeto.login.repositorio.Repositorio;

@Service
public class Servico {

	private Logger logger = LoggerFactory.getLogger(Servico.class);

	@Autowired
	private Repositorio repositorio;

	public Usuario cadastrarUsuario(Usuario usuario) throws UsuarioJahExisteException {

		logger.info("Servico: verificando se {} já foi cadastrado", usuario);

		if (usuarioJahExiste(usuario)) {

			logger.warn("Servico: {} já foi cadastrado", usuario);

			throw new UsuarioJahExisteException();

		}

		logger.info("Servico: Salvando em repositorio {}", usuario);

		return repositorio.save(usuario);

	}

	public Protocolo realizarLogin(Usuario usuario) {

		logger.info("Servico: verificando se {} existe", usuario);

		Usuario usuarioRepositorio = repositorio.usuarioExiste(usuario.getLogin(), usuario.getPassword());

		logger.info("Servico: retornando protocolo de login do {}", usuario);

		return retornarProtocolo(usuarioRepositorio);
	}

	private boolean usuarioJahExiste(Usuario usuario) {
		return repositorio.usuarioJahExiste(usuario.getLogin()) != null;
	}

	private Protocolo retornarProtocolo(Usuario usuario) {
		return usuario == null ? Protocolo.USUARIO_N_CADASTRADO : Protocolo.USUARIO_CADASTRADO;
	}
}
