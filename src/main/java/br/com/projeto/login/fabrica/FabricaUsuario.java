package br.com.projeto.login.fabrica;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.projeto.login.dto.UsuarioDTO;
import br.com.projeto.login.modelo.Usuario;

@Component
public class FabricaUsuario {

	@Autowired
	private final ModelMapper modelMapper;

	public FabricaUsuario(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	public Usuario converter(UsuarioDTO usuarioDTO) {
		return modelMapper.map(usuarioDTO, Usuario.class).usuarioValido();
	}
}
