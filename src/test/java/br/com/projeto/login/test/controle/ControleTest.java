package br.com.projeto.login.test.controle;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.projeto.login.enumeracao.Protocolo;
import br.com.projeto.login.modelo.Usuario;
import br.com.projeto.login.test.UsuarioFabrica;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ControleTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void cadastrarUsuarioTest() {

		// Fabricar Usuario
		Usuario usuario = UsuarioFabrica.fabricar("cadastrarUsuarioTest", "dsaadsdsasadadsads");

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Usuario> usuarioCadastradoResponse = restTemplate.postForEntity(construtorURLCadastro(), usuario,
				Usuario.class);

		/*
		 * Valida se o status recebido é exatamente CREATED, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser OKAY", HttpStatus.CREATED, usuarioCadastradoResponse.getStatusCode());

		Usuario usuarioCadastrado = usuarioCadastradoResponse.getBody();

		/*
		 * Valida se o usuario cadastrado é o mesmo usuario enviado para cadastro
		 */
		Assert.assertEquals("Nome de Login Precisa ser Igual", usuario.getLogin(), usuarioCadastrado.getLogin());
		Assert.assertEquals("Passowrd Precisa ser Igual", usuario.getPassword(), usuarioCadastrado.getPassword());

	}

	@Test
	public void cadastrarUsuarioInvalidoTest() {

		// Fabricar Usuario
		Usuario usuario = UsuarioFabrica.fabricar("", "");

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLCadastro(), usuario,
				Protocolo.class);

		/*
		 * Valida se o status recebido é exatamente BAD_REQUEST, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST,
				protocoloResponse.getStatusCode());

		Protocolo protocolo = protocoloResponse.getBody();

		/*
		 * Valida se o usuario cadastrado é o mesmo usuario enviado para cadastro
		 */
		Assert.assertEquals("Protocolo DEVE SER USUARIO INVALIDO", Protocolo.USUARIO_INVALIDO, protocolo);
		Assert.assertEquals("Protocolo STATUS DEVE SER 1001", Protocolo.USUARIO_INVALIDO.getStatus(),
				protocolo.getStatus());
		Assert.assertEquals("Protocolo INFORMACAO DEVE SER USUARIO_INVALIDO",
				Protocolo.USUARIO_INVALIDO.getInformacao(), protocolo.getInformacao());

	}

	@Test
	public void cadastrarUsuarioInvalido2Test() {

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLCadastro(), null,
				Protocolo.class);

		/*
		 * Valida se o status recebido é exatamente UNSUPPORTED_MEDIA_TYPE, configurado
		 * no serviço
		 */
		Assert.assertEquals("Status precisa ser UNSUPPORTED_MEDIA_TYPE", HttpStatus.UNSUPPORTED_MEDIA_TYPE,
				protocoloResponse.getStatusCode());

		Assert.assertEquals("Body deve vir nulo", null, protocoloResponse.getBody());

	}

	@Test
	public void cadastrarUsuarioInvalido3Test() {

		// Fabricar Usuario
		Usuario usuario = UsuarioFabrica.fabricar(null, null);

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLCadastro(), usuario,
				Protocolo.class);

		/*
		 * Valida se o status recebido é exatamente BAD_REQUEST, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST,
				protocoloResponse.getStatusCode());

		Protocolo protocolo = protocoloResponse.getBody();

		/*
		 * Valida se o usuario cadastrado é o mesmo usuario enviado para cadastro
		 */
		Assert.assertEquals("Protocolo DEVE SER USUARIO INVALIDO", Protocolo.USUARIO_INVALIDO, protocolo);
		Assert.assertEquals("Protocolo STATUS DEVE SER 1001", Protocolo.USUARIO_INVALIDO.getStatus(),
				protocolo.getStatus());
		Assert.assertEquals("Protocolo INFORMACAO DEVE SER USUARIO_INVALIDO",
				Protocolo.USUARIO_INVALIDO.getInformacao(), protocolo.getInformacao());

	}

	@Test
	public void cadastrarUsuarioInvalido4Test() {

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLCadastro(), "",
				Protocolo.class);

		/*
		 * Valida se o status recebido é exatamente UNSUPPORTED_MEDIA_TYPE, configurado
		 * no serviço
		 */
		Assert.assertEquals("Status precisa ser UNSUPPORTED_MEDIA_TYPE", HttpStatus.UNSUPPORTED_MEDIA_TYPE,
				protocoloResponse.getStatusCode());

		Assert.assertEquals("Body deve vir nulo", null, protocoloResponse.getBody());

	}

	@Test
	public void cadastrarUsuario_ExistenteTest() {

		// Fabricar Usuario
		Usuario usuario = UsuarioFabrica.fabricar("cadastrarUsuario_ExistenteTest", "adsdasadsads3443");

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		restTemplate.postForEntity(construtorURLCadastro(), usuario, Object.class);

		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLCadastro(), usuario,
				Protocolo.class);

		/*
		 * Valida se o status recebido é exatamente CONFLICT, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser CONFLICT", HttpStatus.CONFLICT, protocoloResponse.getStatusCode());

		Protocolo protocolo = protocoloResponse.getBody();

		/*
		 * Valida se o usuario cadastrado é o mesmo usuario enviado para cadastro
		 */
		Assert.assertEquals("Protocolo DEVE SER USUARIO JAH CADASTRADO", Protocolo.USUARIO_CADASTRADO, protocolo);
		Assert.assertEquals("Protocolo STATUS DEVE SER 1001", Protocolo.USUARIO_CADASTRADO.getStatus(),
				protocolo.getStatus());
		Assert.assertEquals("Protocolo INFORMACAO DEVE SER USUARIO_CADASTRADO",
				Protocolo.USUARIO_CADASTRADO.getInformacao(), protocolo.getInformacao());

	}

	@Test
	public void loginUsuarioExistenteTest() {

		// Fabricar Usuario
		Usuario usuario = UsuarioFabrica.fabricar("loginUsuarioExistenteTest", "password");

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Usuario> usuarioCadastradoResponse = restTemplate.postForEntity(construtorURLCadastro(), usuario,
				Usuario.class);

		/*
		 * Valida se o status recebido é exatamente CREATED, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser CREATED", HttpStatus.CREATED,
				usuarioCadastradoResponse.getStatusCode());

		Usuario usuarioCadastrado = usuarioCadastradoResponse.getBody();

		/*
		 * Valida se o usuario cadastrado é o mesmo usuario enviado para cadastro
		 */
		Assert.assertEquals("Nome de Login Precisa ser Igual", usuario.getLogin(), usuarioCadastrado.getLogin());
		Assert.assertEquals("Passowrd Precisa ser Igual", usuario.getPassword(), usuarioCadastrado.getPassword());

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta :
		 * Protocolo login
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLLogin(), usuario,
				Protocolo.class);

		Protocolo protocolo = protocoloResponse.getBody();

		/*
		 * Valida se protocolo recebido é de cadastrado
		 */
		Assert.assertEquals("Protocolo DEVE SER USUARIO JAH CADASTRADO", Protocolo.USUARIO_CADASTRADO, protocolo);
		Assert.assertEquals("Protocolo STATUS DEVE SER 1001", Protocolo.USUARIO_CADASTRADO.getStatus(),
				protocolo.getStatus());
		Assert.assertEquals("Protocolo INFORMACAO DEVE SER USUARIO_CADASTRADO",
				Protocolo.USUARIO_CADASTRADO.getInformacao(), protocolo.getInformacao());

	}

	@Test
	public void loginUsuarioInexistenteTest() {

		// Fabricar Usuario
		Usuario usuario = UsuarioFabrica.fabricar("loginUsuarioInexistenteTest", "password");

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta :
		 * Protocolo login
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLLogin(), usuario,
				Protocolo.class);

		Protocolo protocolo = protocoloResponse.getBody();

		/*
		 * Valida se protocolo recebido é de cadastrado
		 */
		Assert.assertEquals("Protocolo DEVE SER USUARIO NAO CADASTRADO", Protocolo.USUARIO_N_CADASTRADO, protocolo);
		Assert.assertEquals("Protocolo STATUS DEVE SER 1001", Protocolo.USUARIO_N_CADASTRADO.getStatus(),
				protocolo.getStatus());
		Assert.assertEquals("Protocolo INFORMACAO DEVE SER USUARIO_N_CADASTRADO",
				Protocolo.USUARIO_N_CADASTRADO.getInformacao(), protocolo.getInformacao());

	}

	@Test
	public void loginUsuarioInvalidoTest() {

		// Fabricar Usuario
		Usuario usuario = UsuarioFabrica.fabricar("", "");

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLLogin(), usuario,
				Protocolo.class);

		/*
		 * Valida se o status recebido é exatamente BAD_REQUEST, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST,
				protocoloResponse.getStatusCode());

		Protocolo protocolo = protocoloResponse.getBody();

		/*
		 * Valida se o usuario cadastrado é o mesmo usuario enviado para cadastro
		 */
		Assert.assertEquals("Protocolo DEVE SER USUARIO INVALIDO", Protocolo.USUARIO_INVALIDO, protocolo);
		Assert.assertEquals("Protocolo STATUS DEVE SER 1001", Protocolo.USUARIO_INVALIDO.getStatus(),
				protocolo.getStatus());
		Assert.assertEquals("Protocolo INFORMACAO DEVE SER USUARIO_INVALIDO",
				Protocolo.USUARIO_INVALIDO.getInformacao(), protocolo.getInformacao());

	}

	@Test
	public void loginUsuarioInvalido2Test() {

		// Fabricar Usuario
		Usuario usuario = UsuarioFabrica.fabricar(null, null);

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLLogin(), usuario,
				Protocolo.class);

		/*
		 * Valida se o status recebido é exatamente BAD_REQUEST, configurado no serviço
		 */
		Assert.assertEquals("Status precisa ser BAD_REQUEST", HttpStatus.BAD_REQUEST,
				protocoloResponse.getStatusCode());

		Protocolo protocolo = protocoloResponse.getBody();

		/*
		 * Valida se o usuario cadastrado é o mesmo usuario enviado para cadastro
		 */
		Assert.assertEquals("Protocolo DEVE SER USUARIO INVALIDO", Protocolo.USUARIO_INVALIDO, protocolo);
		Assert.assertEquals("Protocolo STATUS DEVE SER 1001", Protocolo.USUARIO_INVALIDO.getStatus(),
				protocolo.getStatus());
		Assert.assertEquals("Protocolo INFORMACAO DEVE SER USUARIO_INVALIDO",
				Protocolo.USUARIO_INVALIDO.getInformacao(), protocolo.getInformacao());

	}

	@Test
	public void loginUsuarioInvalido3Test() {

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLLogin(), null,
				Protocolo.class);

		/*
		 * Valida se o status recebido é exatamente UNSUPPORTED_MEDIA_TYPE, configurado
		 * no serviço
		 */
		Assert.assertEquals("Status precisa ser UNSUPPORTED_MEDIA_TYPE", HttpStatus.UNSUPPORTED_MEDIA_TYPE,
				protocoloResponse.getStatusCode());

		Assert.assertEquals("Body deve vir nulo", null, protocoloResponse.getBody());

	}

	@Test
	public void loginUsuarioInvalido4Test() {

		/*
		 * Realizar Requisição do tipo Rest, utilizando verbo post / Resposta : Usuario
		 * cadastrado
		 * 
		 */
		ResponseEntity<Protocolo> protocoloResponse = restTemplate.postForEntity(construtorURLLogin(), "",
				Protocolo.class);

		/*
		 * Valida se o status recebido é exatamente UNSUPPORTED_MEDIA_TYPE, configurado
		 * no serviço
		 */
		Assert.assertEquals("Status precisa ser UNSUPPORTED_MEDIA_TYPE", HttpStatus.UNSUPPORTED_MEDIA_TYPE,
				protocoloResponse.getStatusCode());

		Assert.assertEquals("Body deve vir nulo", null, protocoloResponse.getBody());

	}

	private String construtorURLLogin() {
		return construtorURL() + "login";
	}

	private String construtorURLCadastro() {
		return construtorURL() + "cadastrar";
	}

	private String construtorURL() {
		return "http://localhost:" + port + "/api/";
	}
}
