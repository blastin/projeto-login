<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Projeto Tarefa</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<style>
	@import url("http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");

	body {
		background-color: #3c3c48;
	}

	.ribbon-left {
		background: #FFF;
		color: #000;
		font-size: 16px;
		font-weight: bold;
		padding: 0.625em 3em;
		position: absolute;
		text-decoration: none;
		top: 2.875em;
		z-index: 99999;
		left: -3.125em;
		-webkit-transform: rotate(-45deg);
		-moz-transform: rotate(-45deg);
		-ms-transform: rotate(-45deg);
		-o-transform: rotate(-45deg);
		transform: rotate(-45deg);
	}

	.noselect {
		-webkit-touch-callout: none;
		/* iOS Safari */
		-webkit-user-select: none;
		/* Safari */
		-khtml-user-select: none;
		/* Konqueror HTML */
		-moz-user-select: none;
		/* Firefox */
		-ms-user-select: none;
		/* Internet Explorer/Edge */
		user-select: none;
		/* Non-prefixed version, currently
                                  supported by Chrome and Opera */
	}

	textarea {
		resize: none;
	}

	.chat {
		list-style: none;
		margin: 0;
		padding: 0;
	}

	.chat li {
		margin-bottom: 10px;
		padding-bottom: 5px;
		border-bottom: 1px dotted #B3A9A9;
	}

	.chat li.left .chat-body {
		margin-left: 60px;
	}

	.chat li.right .chat-body {
		margin-right: 60px;
	}

	.chat li .chat-body p {
		margin: 0;
		color: #777777;
	}

	.panel .slidedown .glyphicon,
	.chat .glyphicon {
		margin-right: 5px;
	}

	.body-panel {
		overflow-y: scroll;
		height: 250px;
	}

	::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		background-color: #F5F5F5;
	}

	::-webkit-scrollbar {
		width: 12px;
		background-color: #F5F5F5;
	}

	::-webkit-scrollbar-thumb {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
		background-color: #555;
	}
</style>

<body>

	<a href="https://gitlab.com/blastin/projeto-tarefa" class="ribbon-left" target="_blank">Fork em Gitlab</a>

	<div class="container">
		<div class="jumbotron">
			<h1 class="text-center noselect">Tarefa - DB Com Heroku</h1>
			<p class="text-center noselect">Laborat�rio Banco de Dados II -
				Turma B</p>
		</div>


		<div id=mensagens class="row form-group">
			<div class="col-xs-12 col-md-offset-2 col-md-8 col-lg-8 col-lg-offset-2">
				<div class="panel panel-primary">
					<div class="panel-heading noselect">
						<span class="glyphicon glyphicon-comment"></span> Mensagens
					</div>
					<div class="panel-body body-panel">
						<ul class="chat">

							<c:forEach items="${postagens}" var="postagem">

								<li class="left clearfix"><span class="chat-img pull-left">
										<img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar"
											class="img-circle" />
									</span>
									<div class="chat-body clearfix">
										<div class="header">
											<strong class="primary-font noselect">Anonimo</strong>
										</div>
										<p class="noselect">${postagem.valor}</p>
									</div>
								</li>

							</c:forEach>

						</ul>
					</div>
					<div class="panel-footer clearfix">
						<textarea name="mensagem" class="form-control" rows="3" maxlength="80" required="required"
							placeholder="M�ximo 80 caracteres"></textarea>
						<span class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12"
							style="margin-top: 10px">
							<button class="btn btn-warning btn-lg btn-block" id="btn-chat">Postar</button>
						</span>
					</div>
				</div>
			</div>
		</div>

	</div>

	<script type="text/javascript">
		$(document).ready(function () {
			$("button").click(function () {
				var mensagem = $("textarea").val();
				$.post("/api/mensagem/postar", {
					mensagem: mensagem
				}, function (data, status) {
					if (!!data) {
						location.reload();
						$('textarea[name=mensagem]').val("")
						e.preventDefault();
					}
				});
			});
		});
	</script>

</body>

</html>