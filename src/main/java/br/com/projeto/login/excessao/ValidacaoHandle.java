package br.com.projeto.login.excessao;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.projeto.login.enumeracao.Protocolo;

@ControllerAdvice
public class ValidacaoHandle extends ResponseEntityExceptionHandler {

	private Logger log = LoggerFactory.getLogger(ValidacaoHandle.class);

	@ExceptionHandler
	public ResponseEntity<Protocolo> mensagemInvalida(ConstraintViolationException constraintViolationException) {
		log.warn("Violacao: {}", constraintViolationException.getConstraintViolations().parallelStream().findFirst()
				.orElseThrow(RuntimeException::new).getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Protocolo.USUARIO_INVALIDO);
	}

	@ExceptionHandler
	public ResponseEntity<Protocolo> usuarioJahFoiCadastrado(UsuarioJahExisteException e) {
		log.warn("UsuarioJahExisteException : usuario já foi cadastrado");
		return ResponseEntity.status(HttpStatus.CONFLICT).body(Protocolo.USUARIO_CADASTRADO);
	}

	@ExceptionHandler
	public ResponseEntity<Protocolo> bancoDeDadosCaiu(DataAccessResourceFailureException e) {
		log.warn("DataAccessResourceFailureException : JDBC desconectado");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Protocolo.SERVICO_INDISPONIVEL);
	}
}
