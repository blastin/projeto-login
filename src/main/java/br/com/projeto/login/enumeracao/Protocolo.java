package br.com.projeto.login.enumeracao;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

@JsonFormat(shape = Shape.OBJECT)
public enum Protocolo implements Serializable {

	SERVICO_INDISPONIVEL("SERVICO_INDISPONIVEL", 998), USUARIO_INVALIDO("USUARIO_INVALIDO", 999),
	USUARIO_CADASTRADO("USUARIO_CADASTRADO", 1000), USUARIO_N_CADASTRADO("USUARIO_N_CADASTRADO", 1001);

	private static final String STATUS = "status";

	@JsonProperty
	private String informacao;

	@JsonProperty
	private int status;

	private Protocolo(String informacao, int status) {
		this.informacao = informacao;
		this.status = status;
	}

	public String getInformacao() {
		return informacao;
	}

	public int getStatus() {
		return status;
	}

	@JsonCreator
	public static Protocolo fromNode(JsonNode node) {

		int status = node.get(STATUS).asInt();

		Optional<Protocolo> protocoloPossivel = Arrays.stream(Protocolo.values())
				.filter(protocolo -> protocolo.getStatus() == status).findFirst();

		return protocoloPossivel.orElse(null);
	}

}
