package br.com.projeto.login.controle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.projeto.login.dto.UsuarioDTO;
import br.com.projeto.login.enumeracao.Protocolo;
import br.com.projeto.login.excessao.UsuarioJahExisteException;
import br.com.projeto.login.fabrica.FabricaUsuario;
import br.com.projeto.login.modelo.Usuario;
import br.com.projeto.login.servico.Servico;

@RestController
@RequestMapping("/api/")
public class Controle {

	private Logger logger = LoggerFactory.getLogger(Controle.class);

	@Autowired
	private Servico servico;

	@Autowired
	private FabricaUsuario fabrica;

	@PostMapping("login")
	public Protocolo realizarLogin(@RequestBody UsuarioDTO usuarioDTO) {

		logger.info("Controle : realizando login {}", usuarioDTO);

		Usuario usuario = fabrica.converter(usuarioDTO);

		Protocolo protocolo = servico.realizarLogin(usuario);

		logger.info("Controle<Protocolo> : {}", protocolo);

		return protocolo;

	}

	@PostMapping("cadastrar")
	@ResponseStatus(code = HttpStatus.CREATED)
	public UsuarioDTO cadastrarUsuario(@RequestBody UsuarioDTO usuarioDTO) throws UsuarioJahExisteException {

		Usuario usuario = fabrica.converter(usuarioDTO);

		logger.info("Controle : cadastrando {}", usuario);

		servico.cadastrarUsuario(usuario);

		return usuarioDTO;

	}

}
