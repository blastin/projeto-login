package br.com.projeto.login.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.projeto.login.modelo.Usuario;

public interface Repositorio extends JpaRepository<Usuario, Long> {

	@Query("select u from Usuario u where u.login = ?1")
	Usuario usuarioJahExiste(String login);

	@Query("select u from Usuario u where u.login = ?1 and u.password = ?2")
	Usuario usuarioExiste(String login, String password);
}
