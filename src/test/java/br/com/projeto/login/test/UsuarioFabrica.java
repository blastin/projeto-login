package br.com.projeto.login.test;

import br.com.projeto.login.modelo.Usuario;

public interface UsuarioFabrica {

	public static Usuario fabricar(String login, String password) {
		Usuario usuario = new Usuario();
		usuario.setLogin(login);
		usuario.setPassword(password);
		return usuario;
	}

	public static Usuario fabricar() {
		return fabricar("login", "password");
	}
}
