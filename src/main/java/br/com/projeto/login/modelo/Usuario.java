package br.com.projeto.login.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Usuarios")
public class Usuario extends Violacao<Usuario> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long codigo;

	@NotBlank
	private String login;

	@NotBlank
	@Size(min = 3, max = 40)
	private String password;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getCodigo() {
		return codigo;
	}

	@Override
	public String toString() {
		return "Usuario [codigo=" + codigo + ", login=" + login + ", password=" + password + ", getLogin()="
				+ getLogin() + ", getPassword()=" + getPassword() + ", getCodigo()=" + getCodigo() + "]";
	}

	public Usuario usuarioValido() {
		super.valido();
		return this;
	}
}
